import React, {useEffect, useState, useRef} from 'react';
import {ScrollView} from 'react-native';
import VMasker from 'vanilla-masker';
import DropdownAlert from 'react-native-dropdownalert';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDispatch, useSelector} from 'react-redux';
import {
  Container,
  Content,
  Input,
  Button,
  Header,
  Blood,
} from '../../components';
import {Section, SectionButton} from './styles';
import {updateSolicitation, deleteSolicitation} from '../../redux/solicitation';

export default ({navigation, route}) => {
  const dropDownAlertRef = useRef();

  const dispatch = useDispatch();
  const {
    isFetchUpdateSolicitation,
    isSuccessUpdateSolicitation,
    isErrorUpdateSolicitation,
    isFetchDeleteSolicitation,
    isSuccessDeleteSolicitation,
    isErrorDeleteSolicitation,
    message,
  } = useSelector((state) => state.solicitation);

  const [name, setName] = useState(route.params.selectedDonate.name || '');
  const [address, setAddress] = useState(
    route.params.selectedDonate.address || '',
  );
  const [cellphone, setCellphone] = useState(
    route.params.selectedDonate.cellphone || '',
  );
  const [bloodType, setBloodType] = useState(
    route.params.selectedDonate.bloodType || 'A+',
  );
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (loading) {
      if (isSuccessUpdateSolicitation || isSuccessDeleteSolicitation) {
        navigation.goBack();
        setLoading(false);
      }

      if (isErrorUpdateSolicitation || isErrorDeleteSolicitation) {
        dropDownAlertRef.current.alertWithType(
          'error',
          'Erro de solicitação',
          message,
        );
        setLoading(false);
      }
    }
  }, [
    setLoading,
    isErrorUpdateSolicitation,
    isSuccessUpdateSolicitation,
    isSuccessDeleteSolicitation,
    isErrorDeleteSolicitation,
  ]);

  const onPressConfirm = () => {
    dispatch(
      updateSolicitation({
        name,
        address,
        cellphone,
        bloodType,
        id: route.params.selectedDonate.id,
      }),
    );
    setLoading(true);
  };

  const onPressDelete = () => {
    dispatch(
      deleteSolicitation({
        id: route.params.selectedDonate.id,
      }),
    );
    setLoading(true);
  };

  return (
    <>
      <Container>
        <Header
          onPress={() => navigation.goBack()}
          title="Editar Solicitação"
        />
        <ScrollView>
          <Content>
            <KeyboardAwareScrollView>
              <Section>
                <Input
                  value={name}
                  onChangeText={(value) => setName(value)}
                  label="Nome"
                  autoCapitalize="none"
                  placeholder="Nome do receptor"
                />
                <Input
                  value={address}
                  onChangeText={(value) => setAddress(value)}
                  label="Local da doação"
                  autoCapitalize="none"
                  placeholder="Rua x, número 34"
                />
                <Input
                  value={cellphone}
                  onChangeText={(value) =>
                    setCellphone(VMasker.toPattern(value, '(99) 99999-9999'))
                  }
                  label="Celular (Opcional)"
                  autoCapitalize="none"
                  placeholder="(99) 99999-9999"
                  keyboardType="numeric"
                />
                <Blood
                  value={bloodType}
                  onPress={(value) => setBloodType(value)}
                  label="Selecionar tipo sanguíneo"
                />
              </Section>
              <SectionButton>
                <Button
                  loading={
                    isFetchUpdateSolicitation || isFetchDeleteSolicitation
                  }
                  title="Editar"
                  onPress={onPressConfirm}
                />
                <Button
                  type="transparent"
                  title="Excluir"
                  onPress={onPressDelete}
                />
              </SectionButton>
            </KeyboardAwareScrollView>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};
