import styled from 'styled-components/native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Section = styled.View`
  margin-top: ${hp('4%')}px;
  margin-bottom: ${hp('4%')}px;
`;

export const SectionButton = styled.View``;
