import React, {useEffect, useState, useRef} from 'react';
import {ScrollView} from 'react-native';
import VMasker from 'vanilla-masker';
import DropdownAlert from 'react-native-dropdownalert';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDispatch, useSelector} from 'react-redux';
import {
  Container,
  Content,
  Input,
  Button,
  Header,
  Blood,
  Genre,
} from '../../components';
import {Section, SectionButton} from './styles';
import {singup} from '../../redux/auth';

export default ({navigation}) => {
  const dropDownAlertRef = useRef();

  const dispatch = useDispatch();
  const {isFetchSingup, isSuccessSingup, isErrorSingup, message} = useSelector(
    (state) => state.auth,
  );

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPasswoard] = useState('');
  const [dateBirth, setDateBirth] = useState('');
  const [weight, setWeight] = useState('');
  const [cellphone, setCellphone] = useState('');
  const [genre, setGenre] = useState('M');
  const [bloodType, setBloodType] = useState('A+');
  const [pressSingup, setPressSingup] = useState('');

  useEffect(() => {
    if (pressSingup) {
      if (isSuccessSingup) {
        navigation.navigate('Login');
        setPressSingup(false);
      }

      if (isErrorSingup) {
        dropDownAlertRef.current.alertWithType(
          'error',
          'Erro de cadastro',
          message,
        );
        setPressSingup(false);
      }
    }
  }, [pressSingup, isErrorSingup, isSuccessSingup]);

  const onPressLogin = () => {
    dispatch(
      singup({
        name,
        email,
        password,
        cellphone,
        weight,
        bloodType,
        genre,
        dateBirth: dateBirth.split('/').reverse().join('-'),
      }),
    );
    setPressSingup(true);
  };

  return (
    <>
      <Container>
        <Header onPress={() => navigation.goBack()} title="Torne-se Doador" />
        <ScrollView>
          <Content>
            <KeyboardAwareScrollView>
              <Section>
                <Input
                  value={name}
                  onChangeText={(value) => setName(value)}
                  label="Nome"
                  autoCapitalize="none"
                  placeholder="Seu nome"
                />
                <Input
                  value={email}
                  onChangeText={(value) => setEmail(value)}
                  label="Email"
                  autoCapitalize="none"
                  placeholder="exemplo@email.com"
                />
                <Input
                  secureTextEntry
                  onChangeText={(value) => setPasswoard(value)}
                  value={password}
                  label="Senha"
                  autoCapitalize="none"
                  placeholder="******"
                />
                <Input
                  value={dateBirth}
                  onChangeText={(value) =>
                    setDateBirth(VMasker.toPattern(value, '99/99/9999'))
                  }
                  label="Data de Nascimento"
                  autoCapitalize="none"
                  placeholder="99/99/9999"
                  keyboardType="numeric"
                />
                <Input
                  value={weight}
                  onChangeText={(value) => setWeight(value)}
                  label="Peso"
                  autoCapitalize="none"
                  placeholder="99"
                  keyboardType="numeric"
                />
                <Input
                  value={cellphone}
                  onChangeText={(value) =>
                    setCellphone(VMasker.toPattern(value, '(99) 99999-9999'))
                  }
                  label="Celular"
                  autoCapitalize="none"
                  placeholder="(99) 99999-9999"
                  keyboardType="numeric"
                />
                <Genre
                  label="Gênero"
                  value={genre}
                  onPress={(value) => setGenre(value)}
                />
                <Blood
                  value={bloodType}
                  onPress={(value) => setBloodType(value)}
                  label="Selecionar tipo sanguíneo"
                />
              </Section>
              <SectionButton>
                <Button
                  loading={isFetchSingup}
                  title="Cadastre-se"
                  onPress={onPressLogin}
                />
              </SectionButton>
            </KeyboardAwareScrollView>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};
