import React from 'react';
import {ScrollView, Text} from 'react-native';
import {Container, Content, Header, Title, Button} from '../../components';
import {Section, SectionButton} from './styles';
import {theme} from '../../resources';

export default ({navigation, route}) => {
  return (
    <>
      <Container>
        <Header onPress={() => navigation.goBack()} title="Informações" />
        <ScrollView>
          <Content>
            <Section>
              <Title color={theme.thirdColor} size="small" align="left">
                {`Cuidados a serem tomados:

•	Estar bem alimentado e evitar comidas gordurosas 3 horas antes da doação.
•	Caso tenha tido uma refeição reforçada, como um almoço, aguardar 2 horas para realizar a doação.
•	Ter dormido ao menos 6 horas nas últimas 24 horas.
•	Pessoas com idades entre 60-69 anos só poderão realizar doação caso já tenham feito antes deste período.
•	Anualmente homens podem doar 4 vezes, com um intervalo de dois meses entre as doações e as mulheres 3 vezes com um intervalo de 3 meses entre as doações.
•	Peso superior a 50kg.
•	Não poderá realizar a doação caso tenha febre no dia da mesma. (Ministério da Saúde)

Restrições definitivas:

•	Ter hepatite após os 10 anos de idade.
•	Ter algum tipo de doença transmitida pelo sangue.
•	Uso de drogas ilícitas injetáveis.
•	Malária.

Restrições temporárias:

•	Período gestacional e pós-gravidez: 90 dias após parto normal ou 180 dias após parto cesariana.
•	Caso esteja amamentando: 12 meses após parto.
•	Ingestão de bebida alcoólica: 12 horas.
•	Tatuagem ou piercing: 12 meses.
•	Extração de dente: 72 horas.
•	Apendicite, hérnia, amigdalectomia e varizes: 3 meses.
•	Colecistectómica, histerectomia, nefrectomia, redução de fraturas, politraumatismos sem sequelas graves, tireoidectómica, colectomia: 6 meses.
•	Transfusão de sangue: 1 ano.
•	Vacinação: depende da vacina, para mais informações consultar o local de doação.
•	Exames com endoscópios: 6 meses.
•	Ter sido exposto a alguma situação de risco, incluindo infecções sexualmente transmissíveis: 12 meses.`}
              </Title>
            </Section>
            <SectionButton>
              <Button
                onPress={() =>
                  navigation.navigate('InfoDonor', {
                    selectedDonate: {...route.params.selectedDonate},
                  })
                }
                title="Confirmar"
              />
            </SectionButton>
          </Content>
        </ScrollView>
      </Container>
    </>
  );
};
