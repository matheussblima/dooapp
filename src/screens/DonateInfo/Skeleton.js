import React from 'react';
import {View} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default () => {
  return (
    <SkeletonPlaceholder>
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
      <View style={{width: '100%', height: 100, marginBottom: 8}} />
    </SkeletonPlaceholder>
  );
};
