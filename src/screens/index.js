import Login from './Login';
import Singup from './Singup';
import Home from './Home';
import Request from './Request';
import Donate from './Donate';
import InfoDonor from './InfoDonor';
import Solicitations from './Solicitations';
import EditSolicitation from './EditSolicitation';
import DonateInfo from './DonateInfo';
import GeneralInformation from './GeneralInformation';

export {
  Login,
  Singup,
  Home,
  Request,
  Donate,
  InfoDonor,
  Solicitations,
  EditSolicitation,
  DonateInfo,
  GeneralInformation,
};
