import React, {useEffect, useState, useRef} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import DropdownAlert from 'react-native-dropdownalert';
import {useDispatch, useSelector} from 'react-redux';
import {Container, Content, Logo, Input, Button} from '../../components';
import {theme} from '../../resources';
import {Header, Section, SectionButton} from './styles';
import {login} from '../../redux/auth';

export default (props) => {
  const dropDownAlertRef = useRef();

  const dispatch = useDispatch();
  const {isFetchAuth, isSuccessAuth, isErrorAuth, message} = useSelector(
    (state) => state.auth,
  );

  const [email, setEmail] = useState('');
  const [password, setPasswoard] = useState('');
  const [pressLogin, setPressLogin] = useState(false);

  useEffect(() => {
    if (pressLogin) {
      if (isSuccessAuth) {
        props.navigation.navigate('Home');
        setPressLogin(false);
      }

      if (isErrorAuth) {
        dropDownAlertRef.current.alertWithType(
          'error',
          'Erro de autenticação',
          message,
        );
        setPressLogin(false);
      }
    }
  }, [pressLogin, isErrorAuth, isSuccessAuth]);

  const onPressLogin = () => {
    dispatch(login(email, password));
    setPressLogin(true);
  };

  return (
    <>
      <Container>
        <Header colors={[theme.fourthColor, theme.fifthColor]}>
          <Logo />
        </Header>
        <Content>
          <KeyboardAwareScrollView>
            <Section>
              <Input
                value={email}
                onChangeText={(value) => setEmail(value)}
                label="Email"
                autoCapitalize="none"
                placeholder="exemplo@email.com"
              />
              <Input
                secureTextEntry
                onChangeText={(value) => setPasswoard(value)}
                value={password}
                label="Senha"
                autoCapitalize="none"
                placeholder="******"
              />
            </Section>
            <SectionButton>
              <Button
                loading={isFetchAuth}
                title="Login"
                onPress={onPressLogin}
              />
              <Button
                onPress={() => props.navigation.navigate('Singup')}
                type="transparent"
                title="Cadastre-se"
              />
            </SectionButton>
            <SectionButton>
              <Button
                onPress={() => props.navigation.navigate('GeneralInformation')}
                type="transparent"
                title="Informações Gerais"
              />
            </SectionButton>
          </KeyboardAwareScrollView>
        </Content>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};
