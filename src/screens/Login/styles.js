import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const Header = styled(LinearGradient)`
  width: 100%;
  height: ${hp('16%')}px;
  justify-content: center;
  align-items: center;
`;

export const Section = styled.View`
  margin-top: ${hp('8%')}px;
  margin-bottom: ${hp('4%')}px;
`;

export const SectionButton = styled.View``;

export const SectionButtonInfo = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;
