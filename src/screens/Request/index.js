import React, {useEffect, useState, useRef} from 'react';
import {ScrollView} from 'react-native';
import VMasker from 'vanilla-masker';
import DropdownAlert from 'react-native-dropdownalert';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useDispatch, useSelector} from 'react-redux';
import {
  Container,
  Content,
  Input,
  Button,
  Header,
  Blood,
} from '../../components';
import {Section, SectionButton} from './styles';
import {setSolicitation} from '../../redux/solicitation';

export default ({navigation}) => {
  const dropDownAlertRef = useRef();

  const dispatch = useDispatch();
  const {
    isFetchSetSolicitation,
    isSuccessSetSolicitation,
    isErrorSetSolicitation,
    message,
  } = useSelector((state) => state.solicitation);

  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [cellphone, setCellphone] = useState('');
  const [bloodType, setBloodType] = useState('A+');
  const [loading, setLoading] = useState('');

  useEffect(() => {
    if (loading) {
      if (isSuccessSetSolicitation) {
        navigation.goBack();
        setLoading(false);
      }

      if (isErrorSetSolicitation) {
        dropDownAlertRef.current.alertWithType(
          'error',
          'Erro de solicitação',
          message,
        );
        setLoading(false);
      }
    }
  }, [setLoading, isErrorSetSolicitation, isSuccessSetSolicitation]);

  const onPressConfirm = () => {
    dispatch(
      setSolicitation({
        name,
        address,
        cellphone,
        bloodType,
      }),
    );
    setLoading(true);
  };

  return (
    <>
      <Container>
        <Header onPress={() => navigation.goBack()} title="Solicitar" />
        <ScrollView>
          <Content>
            <KeyboardAwareScrollView>
              <Section>
                <Input
                  value={name}
                  onChangeText={(value) => setName(value)}
                  label="Nome"
                  autoCapitalize="none"
                  placeholder="Nome do receptor"
                />
                <Input
                  value={address}
                  onChangeText={(value) => setAddress(value)}
                  label="Local da doação"
                  autoCapitalize="none"
                  placeholder="Rua x, número 34"
                />
                <Input
                  value={cellphone}
                  onChangeText={(value) =>
                    setCellphone(VMasker.toPattern(value, '(99) 99999-9999'))
                  }
                  label="Celular (Opcional)"
                  autoCapitalize="none"
                  placeholder="(99) 99999-9999"
                  keyboardType="numeric"
                />
                <Blood
                  value={bloodType}
                  onPress={(value) => setBloodType(value)}
                  label="Selecionar tipo sanguíneo"
                />
              </Section>
              <SectionButton>
                <Button
                  loading={isFetchSetSolicitation}
                  title="Confirmar"
                  onPress={onPressConfirm}
                />
              </SectionButton>
            </KeyboardAwareScrollView>
          </Content>
        </ScrollView>
      </Container>
      <DropdownAlert ref={dropDownAlertRef} />
    </>
  );
};
