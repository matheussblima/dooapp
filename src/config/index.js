import Routes from './routes';
import texts from './texts';
import api from './api';

export {Routes, texts, api};
