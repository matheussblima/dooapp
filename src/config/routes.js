import * as React from 'react';
import {useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Login,
  Singup,
  Home,
  Request,
  Donate,
  InfoDonor,
  Solicitations,
  GeneralInformation,
  DonateInfo,
  EditSolicitation,
} from '../screens';

const Stack = createStackNavigator();

function App(props) {
  const {isSuccessAuth} = useSelector((state) => state.auth);

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={isSuccessAuth ? 'Home' : 'Login'}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Singup" component={Singup} />
        <Stack.Screen
          name="GeneralInformation"
          component={GeneralInformation}
        />
        <Stack.Screen name="DonateInfo" component={DonateInfo} />
        <Stack.Screen name="Donate" component={Donate} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Request" component={Request} />
        <Stack.Screen name="InfoDonor" component={InfoDonor} />
        <Stack.Screen name="Solicitations" component={Solicitations} />
        <Stack.Screen name="EditSolicitation" component={EditSolicitation} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
