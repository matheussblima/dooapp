import React, {Component} from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {ThemeProvider} from 'styled-components/native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import configureStore, {persistor} from './redux/store';
import {Routes} from './config';
import {theme} from './resources';

const store = configureStore();

//Loading of the persist
const LoadingView = () => {
  return (
    <View style={styles.loading}>
      <ActivityIndicator size="small" color="#000" />
    </View>
  );
};

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <PersistGate loading={<LoadingView />} persistor={persistor}>
            <Routes />
          </PersistGate>
        </Provider>
      </ThemeProvider>
    );
  }
}

const styles = StyleSheet.create({
  loading: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default App;
