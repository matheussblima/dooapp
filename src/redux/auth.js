import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';

// Actions
const AUTH_REQUEST = 'dooApp/auth/AUTH_REQUEST';
const AUTH_SUCCESS = 'dooApp/auth/AUTH_SUCCESS';
const AUTH_FAILURE = 'dooApp/auth/AUTH_FAILURE';

const SINGUP_REQUEST = 'dooApp/auth/SINGUP_REQUEST';
const SINGUP_SUCCESS = 'dooApp/auth/SINGUP_SUCCESS';
const SINGUP_FAILURE = 'dooApp/auth/SINGUP_FAILURE';

const initialState = {
  profile: {
    emptyFields: [],
  },
  auth: {data: {}},
};

// Reducer
function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case AUTH_REQUEST:
      return {
        ...state,
        isFetchAuth: true,
        isSuccessAuth: false,
        isErrorAuth: false,
      };
    case AUTH_SUCCESS:
      return {
        ...state,
        isFetchAuth: false,
        isSuccessAuth: true,
        isErrorAuth: false,
        auth: action.payload,
      };
    case AUTH_FAILURE:
      return {
        ...state,
        isFetchAuth: false,
        isSuccessAuth: false,
        isErrorAuth: true,
        message: action.message,
      };

    case SINGUP_REQUEST:
      return {
        ...state,
        isFetchSingup: true,
        isSuccessSingup: false,
        isErrorSingup: false,
      };
    case SINGUP_SUCCESS:
      return {
        ...state,
        isFetchSingup: false,
        isSuccessSingup: true,
        isErrorSingup: false,
        singup: action.payload,
      };
    case SINGUP_FAILURE:
      return {
        ...state,
        isFetchSingup: false,
        isSuccessSingup: false,
        isErrorSingup: true,
        message: action.message,
      };

    default:
      return state;
  }
}

const persistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  blacklist: [
    'message',
    'isSuccessSingup',
    'isErrorSingup',
    'isFetchSingup',
    'isSuccessAuth',
    'isFetchAuth',
    'isErrorAuth',
  ],
};

export const login = (email, password) => (dispatch) => {
  dispatch({type: AUTH_REQUEST});

  if (!email || !password) {
    return dispatch({
      type: AUTH_FAILURE,
      message: 'Verifique os dados digitados',
    });
  }

  auth()
    .signInWithEmailAndPassword(email, password)
    .then((data) => {
      return dispatch({type: AUTH_SUCCESS, payload: data});
    })
    .catch((error) => {
      return dispatch({
        type: AUTH_FAILURE,
        message: 'Erro ao fazer login, tente novamente',
      });
    });
};

export const singup = ({
  email,
  password,
  name,
  dateBirth,
  weight,
  cellphone,
  genre,
  bloodType,
}) => (dispatch) => {
  dispatch({type: SINGUP_REQUEST});

  if (
    !email ||
    !password ||
    !name ||
    !dateBirth ||
    !weight ||
    !cellphone ||
    !genre ||
    !bloodType
  ) {
    return dispatch({
      type: SINGUP_FAILURE,
      message: 'Verifique os dados digitados',
    });
  }

  auth()
    .createUserWithEmailAndPassword(email, password)
    .then((dataSingup) => {
      const user = firestore().collection('users');
      user
        .add({
          userId: dataSingup.user.uid,
          name,
          dateBirth,
          weight,
          cellphone,
          genre,
          bloodType,
        })
        .then((dataUser) => {
          return dispatch({
            type: SINGUP_SUCCESS,
            payload: {
              singup: dataSingup,
              user: {
                userId: dataSingup.user.uid,
                name,
                dateBirth,
                weight,
                cellphone,
                genre,
                bloodType,
              },
            },
          });
        })
        .catch((error) => {
          return dispatch({
            type: SINGUP_FAILURE,
            message: 'Erro ao criar usuário, tente novamente',
          });
        });
    })
    .catch((error) => {
      return dispatch({
        type: SINGUP_FAILURE,
        message: 'Erro ao criar usuário, tente novamente',
      });
    });
};

export default persistReducer(persistConfig, reducer);
