import React from 'react';
import styled from 'styled-components/native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {theme} from '../../resources';

const ContainerInput = styled.View`
  height: 54px;
  background-color: ${(props) =>
    props.backgroundColor || `${props.theme.sixthColor}`};
  border-radius: ${(props) => props.radius || `${wp('2%')}px`};
  justify-content: center;
  margin-top: 8px;
  margin-bottom: 8px;
`;

const Container = styled.View`
  margin-top: ${(props) => props.marginTop}px;
  margin-left: ${(props) => props.marginLeft}px;
  margin-right: ${(props) => props.marginRigth}px;
  margin-bottom: ${(props) => props.marginBottom}px;
`;

const TextInput = styled.TextInput`
  width: 100%;
  height: 54px;
  font-size: ${(props) => props.fontSize || wp('5%')}px;
  color: ${(props) => props.color || props.theme.secondColor};
`;

const Label = styled.Text`
  font-size: ${(props) => props.fontSize || wp('5%')}px;
  color: ${(props) => props.color || props.theme.thirdColor};
  font-weight: bold;
  margin-left: ${(props) => props.marginLeft}px;
`;

const Input = (props) => {
  return (
    <Container
      marginLeft={props.marginLeft}
      marginRigth={props.marginRigth}
      marginTop={props.marginTop}
      marginBottom={props.marginBottom}>
      <Label marginLeft={props.marginLeft}>{props.label}</Label>
      <ContainerInput>
        <TextInput
          color={props.color}
          placeholderTextColor={theme.eighthColor}
          {...props}
          underlineColorAndroid="rgba(0,0,0,0)"
        />
      </ContainerInput>
    </Container>
  );
};

Input.defaultProps = {
  marginBottom: 8,
  marginTop: 8,
  marginRigth: 8,
  marginLeft: 8,
};

export default Input;
