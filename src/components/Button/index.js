import React from 'react';
import {ActivityIndicator} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import styled, {css} from 'styled-components/native';
import {theme} from '../../resources';

const TouchableOpacity = styled.TouchableOpacity`
  border-color: ${(props) => props.borderColor || 'transparent'}; 
  background-color: ${(props) =>
    props.backgroundColor || props.theme.primaryColor};
  border-width:  2px;


  padding-top: ${(props) => props.paddingTop || '8px'};
  padding-left: ${(props) => props.paddingLeft || '24px'};
  padding-right: ${(props) => props.paddingRight || '24px'};
  padding-bottom: ${(props) => props.paddingBottom || '8px'};

  margin-top: ${(props) => props.marginTop}px;
  margin-left: ${(props) => props.marginLeft}px;
  margin-right: ${(props) => props.marginRigth}px;
  margin-bottom: ${(props) => props.marginBottom}px;

  align-items: ${(props) => props.align || 'center'};
  justify-content: center;

    ${(props) =>
      props.type === 'transparent' &&
      css`
        background-color: transparent;
        border-color: transparent;
      `}
    ${(props) =>
      props.type === 'rounded' &&
      css`
        height: 60px;

        border-radius: ${props.radius || `${wp('20%')}px`};
      `}
    ${(props) =>
      props.type === 'normal' &&
      css`
        height: 60px;
        border-radius: ${props.radius || `${wp('2%')}px`};
      `}
    ${(props) =>
      props.type === 'normal' &&
      css`
        height: 60px;
        border-radius: ${props.radius || `${wp('2%')}px`};
      `}
    ${(props) =>
      props.type === 'circle' &&
      css`
        height: 60px;
        width: 60px;
        border-radius: 30px;
        background-color: ${props.backgroundColor || props.theme.secondColor};
        background-color: ${props.backgroundColor || props.theme.secondColor};
      `};
`;

const Text = styled.Text`
  ${(props) =>
    props.type === 'normal' &&
    css`
      color: ${props.color || props.theme.secondColor};
    `}

  ${(props) =>
    props.type === 'rounded' &&
    css`
      color: ${props.color || props.theme.secondColor};
    `}

  ${(props) =>
    props.type === 'transparent' &&
    css`
      color: ${props.color || props.theme.primaryColor};
      text-decoration: underline;
      text-decoration-color: ${props.color || props.theme.primaryColor};
    `}

  text-transform: ${(props) => props.textTransform || 'uppercase'};
  font-weight: ${(props) => props.fontWeight || 'normal'};
  font-size: ${hp('2%')}px;
`;

const Icons = styled.Image`
  height: 32px;
  width: 32px;
  tint-color: ${(props) => props.tintColor};
`;

const Content = styled.View`
  flex-direction: row;
`;

const Button = (props) => {
  return (
    <TouchableOpacity disabled={props.loading} {...props} activeOpacity={0.8}>
      {props.loading ? (
        <ActivityIndicator size="small" color={theme.secondColor} />
      ) : (
        <Content>
          {props.iconLeft ? (
            <Icons
              tintColor={props.tintColor}
              source={props.iconLeft}
              resizeMode="contain"
            />
          ) : undefined}
          <Text {...props}>{props.title}</Text>
          {props.iconRight ? (
            <Icons
              source={props.iconRight}
              tintColor={props.tintColor}
              resizeMode="contain"
            />
          ) : undefined}
        </Content>
      )}
    </TouchableOpacity>
  );
};

Button.defaultProps = {
  type: 'normal',
  marginBottom: 8,
  marginTop: 8,
  marginRigth: 8,
  marginLeft: 8,
};

export default Button;
