import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styled from 'styled-components/native';
import {images} from '../../resources';

const Image = styled.Image`
  height: ${(props) => props.height || wp('16%')}px;
  width: ${(props) => props.width || wp('50%')}px;
`;

const Logo = (props) => {
  return <Image {...props} source={images.logo} resizeMode="contain" />;
};

export default Logo;
