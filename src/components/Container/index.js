import styled from 'styled-components/native';

export default styled.SafeAreaView`
  padding: 10px 0px;
  flex: 1;
  background-color: ${(props) =>
    props.backgroundColor || props.theme.ninethColor};
`;
