import React from 'react';
import styled from 'styled-components/native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

const Text = styled.Text`
  font-size: ${(props) => props.size || hp('6%')}px;
  font-weight: bold;
  color: ${(props) => props.theme.sixthColor};
  text-transform: uppercase;
`;

const Container = styled.View`
  width: ${(props) => props.size || wp('34%')}px;
  height: ${(props) => props.size || wp('34%')}px;
  border-radius: ${(props) => props.size || wp('34%')}px;
  background-color: ${(props) => props.theme.fifthColor};
  margin-top: ${hp('1%')}px;
  align-items: center;
  align-content: center;
  justify-content: center;
`;

const Image = styled.Image`
  width: ${(props) => props.size || wp('34%')}px;
  height: ${(props) => props.size || wp('34%')}px;
  border-radius: ${(props) => props.size || wp('34%')}px;
  margin-right: ${(props) => props.marginRight || wp('0%')}px;
`;

const Avatar = (props) => {
  return (
    <Container {...props}>
      {props.source ? (
        <Image size={props.size} source={props.source} />
      ) : (
        <Text size={props.sizeText}>{props.initialsName}</Text>
      )}
    </Container>
  );
};

export default Avatar;
